package elgato;
import static java.lang.Math.random;

import javax.swing.JOptionPane;
public class elGato extends javax.swing.JFrame {
String Equis = "X";
    public elGato() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        NG = new javax.swing.JButton();
        TiroMaquina = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(new java.awt.GridLayout(3, 3));

        jButton1.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);

        jButton2.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);

        jButton3.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);

        jButton4.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4);

        jButton5.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton5);

        jButton6.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton6);

        jButton7.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton7);

        jButton8.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton8);

        jButton9.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton9);

        NG.setText("Nuevo juego");
        NG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NGActionPerformed(evt);
            }
        });

        TiroMaquina.setText("Tiro de la maquina");
        TiroMaquina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TiroMaquinaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 381, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(79, 79, 79)
                        .addComponent(NG)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(TiroMaquina)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TiroMaquina)
                    .addComponent(NG))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        jButton1.setText(Equis);
        if(Equis.equals("X")){
            Equis = "O";
        }
        else{
            Equis = "X";
        }
        Ganador();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        jButton2.setText(Equis);
        if(Equis.equals("X")){
            Equis = "O";
        }
        else{
            Equis = "X";
        }
        Ganador();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        jButton3.setText(Equis);
        if(Equis.equals("X")){
            Equis = "O";
        }
        else{
            Equis = "X";
        }
        Ganador();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        jButton4.setText(Equis);
        if(Equis.equals("X")){
            Equis = "O";
        }
        else{
            Equis = "X";
        }
        Ganador();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        jButton5.setText(Equis);
        if(Equis.equals("X")){
            Equis = "O";
        }
        else{
            Equis = "X";
        }
        Ganador();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        jButton6.setText(Equis);
        if(Equis.equals("X")){
            Equis = "O";
        }
        else{
            Equis = "X";
        }
        Ganador();
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed

        jButton7.setText(Equis);
        if(Equis.equals("X")){
            Equis = "O";
        }
        else{
            Equis = "X";
        }
        Ganador();
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        jButton8.setText(Equis);
        if(Equis.equals("X")){
            Equis = "O";
        }
        else{
            Equis = "X";
        }
        Ganador();
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        jButton9.setText(Equis);
        if(Equis.equals("X")){
            Equis = "O";
        }
        else{
            Equis = "X";
        }
        Ganador();
    }//GEN-LAST:event_jButton9ActionPerformed

    private void NGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NGActionPerformed
        jButton1.setText("");
        jButton2.setText("");
        jButton3.setText("");
        jButton4.setText("");
        jButton5.setText("");
        jButton6.setText("");
        jButton7.setText("");
        jButton8.setText("");
        jButton9.setText("");
    }//GEN-LAST:event_NGActionPerformed

    private void TiroMaquinaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TiroMaquinaActionPerformed
        boolean resp = false;
        String uno = jButton1.getText();
        String dos = jButton2.getText();
        String tres = jButton3.getText();
        String cuatro = jButton4.getText();
        String cinco = jButton5.getText();
        String seis = jButton6.getText();
        String siete = jButton7.getText();
        String ocho = jButton8.getText();
        String nueve = jButton9.getText();
        do{
            int numerorandom = (int)(Math.random()*9+1);
            switch(numerorandom){
                case 1:{ if(uno!="X"&&uno!="O"){jButton1.setText("O"); resp=true;} Ganador();}
                break;
                case 2:{ if(dos!="X"&&dos!="O"){jButton2.setText("O"); resp=true;} Ganador();}
                break;
                case 3:{ if(tres!="X"&&tres!="O"){jButton3.setText("O"); resp=true;} Ganador();}
                break;
                case 4:{ if(cuatro!="X"&&cuatro!="O"){jButton4.setText("O"); resp=true;} Ganador();}
                break;
                case 5:{ if(cinco!="X"&&cinco!="O"){jButton5.setText("O"); resp=true;} Ganador();}
                break;
                case 6:{ if(seis!="X"&&seis!="O"){jButton6.setText("O"); resp=true;} Ganador();}
                break;
                case 7:{ if(siete!="X"&&siete!="O"){jButton7.setText("O"); resp=true;} Ganador();}
                break;
                case 8:{ if(ocho!="X"&&ocho!="O"){jButton8.setText("O"); resp=true;} Ganador();}
                break;
                case 9:{ if(nueve!="X"&&nueve!="O"){jButton9.setText("O"); resp=true;} Ganador();}
                break;
            }
        }while(resp!=true);
    }//GEN-LAST:event_TiroMaquinaActionPerformed
private void Ganador(){
        String uno = jButton1.getText();
        String dos = jButton2.getText();
        String tres = jButton3.getText();
        String cuatro = jButton4.getText();
        String cinco = jButton5.getText();
        String seis = jButton6.getText();
        String siete = jButton7.getText();
        String ocho = jButton8.getText();
        String nueve = jButton9.getText();
        
        
        
        if(uno.equals("X")&& dos.equals("X")&& tres.equals("X")){
            GanoX();
        } 
            else if(uno.equals("O")&& dos.equals("O")&& tres.equals("O"))
                {
                    GanoO();
                }
                else if(cuatro.equals("X")&& cinco.equals("X")&& seis.equals("X")){
                    GanoX();
                } 
                    else if(cuatro.equals("O")&& cinco.equals("O")&& seis.equals("O"))
                        {
                            GanoO();
                        }
                        else if(siete.equals("X")&& ocho.equals("X")&& nueve.equals("X")){
                                GanoX();
                            } 
                                else if(siete.equals("O")&& ocho.equals("O")&& nueve.equals("O"))
                                    {
                                        GanoO();
                                    }
                                    else if(uno.equals("X")&& cuatro.equals("X")&& siete.equals("X")){
                                        GanoX();
                                     } 
                                        else if(uno.equals("O")&& cuatro.equals("O")&& siete.equals("O"))
                                            {
                                                GanoO();
                                            }
                                            else if(dos.equals("X")&& cinco.equals("X")&& ocho.equals("X")){
                                            GanoX();
                                            } 
                                                else if(dos.equals("O")&& cinco.equals("O")&& ocho.equals("O"))
                                                    {
                                                        GanoO();
                                                    }
                                                    else if(tres.equals("X")&& seis.equals("X")&& nueve.equals("X")){
                                                    GanoX();
                                                    } 
                                                        else if(tres.equals("O")&& seis.equals("O")&& nueve.equals("O"))
                                                            {
                                                                GanoO();
                                                            }
                                                        else if(uno.equals("X")&& cinco.equals("X")&& nueve.equals("X")){
                                                            GanoX();
                                                        } 
                                                            else if(uno.equals("O")&& cinco.equals("O")&& nueve.equals("O"))
                                                                {
                                                                    GanoO();
                                                                }
                                                                else if(tres.equals("X")&& cinco.equals("X")&& siete.equals("X")){
                                                                GanoX();
                                                                } 
                                                                    else if(tres.equals("O")&& cinco.equals("O")&& siete.equals("O"))
                                                                        {
                                                                            GanoO();
                                                                        }
                                                                        else{
                                                                            Empate();
                                                                        }
        
                                                                        
    }
    
    private void GanoX(){
        JOptionPane.showMessageDialog(null, "X es ganador!");
        jButton1.setText("");
        jButton2.setText("");
        jButton3.setText("");
        jButton4.setText("");
        jButton5.setText("");
        jButton6.setText("");
        jButton7.setText("");
        jButton8.setText("");
        jButton9.setText("");
    }
    
    private void GanoO(){
        JOptionPane.showMessageDialog(null, "O es ganador!");
        jButton1.setText("");
        jButton2.setText("");
        jButton3.setText("");
        jButton4.setText("");
        jButton5.setText("");
        jButton6.setText("");
        jButton7.setText("");
        jButton8.setText("");
        jButton9.setText("");
    }
    
    private void Empate(){
        
        String uno = jButton1.getText();
        String dos = jButton2.getText();
        String tres = jButton3.getText();
        String cuatro = jButton4.getText();
        String cinco = jButton5.getText();
        String seis = jButton6.getText();
        String siete = jButton7.getText();
        String ocho = jButton8.getText();
        String nueve = jButton9.getText();
        
        if(uno!=""&&dos!=""&&tres!=""&&cuatro!=""&&cinco!=""&&seis!=""&&siete!=""&&ocho!=""&&nueve!=""){
                JOptionPane.showMessageDialog(null, "Esto fue un empate");
    
    
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ElGato1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ElGato1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ElGato1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ElGato1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new elGato().setVisible(true);
            }
        });
    }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton NG;
    private javax.swing.JButton TiroMaquina;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
